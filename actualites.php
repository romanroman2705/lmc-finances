<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Actualités LMC</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.1/css/bulma.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="https://kit.fontawesome.com/22fdf35712.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="scripts/scripts.js"></script>
  </head>

<body>
    <section class="hero is-fullheight is-dark hero-actualites-presentation">
        <div class="hero-head">
            <?php require 'menu.php'; ?>
        </div>
        <div class="hero-body">
          <div class="container">
              <div class="columns is-justify-content-start">
                <div class="column is-three-fifths">
                  <h1 class="is-size-big is-size-1-mobile has-text-fuchsia has-text-weight-bold"> Les actualités </h1>
                  <h1 class="is-size-big is-size-1-mobile has-text-weight-bold">du monde des finances</h1>
                  <p class="subtitle">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Assumenda 
                    consequatur impedit nobis similique adipisci quibusdam, 
                    nam enim doloribus dolore, quo quas ducimus nesciunt earum, dolorem qui eum reprehenderit. Nostrum, ipsam.</p>
                </div>
              </div>
            </div>
        </div>
        <div class="hero-footer">

        </div>
    </section>

    <!-- SECTION ACTUALITES -->

    <section class="hero is-fullheight">
      
        <div class="hero-body">
            <div class="container">
                <div class="columns">
                    <div class="column is-flex is-flex-direction-column is-align-items-center is-justify-content-center">
                        <h1 class="title has-text-centered">
                            Les finances de nos jours
                        </h1>
                        <div class="underline-fuchsia"></div>
                    </div>
                </div>
                <div class="columns">
                    <div class="column">
                        <figure class="image is-4by3">
                            <img src="https://bulma.io/images/placeholders/640x480.png" alt="">
                        </figure>
                        <h1 class="title mt-3"> Article recent titre</h1>
                        <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellat, deserunt nobis vitae dolor tenetur amet, rem totam inventore provident voluptatibus iure cupiditate fugit nemo numquam, tempora in. Non, voluptatem deleniti!
                        </p>
                    </div>
                    <div class="column others-articles is-align-self-flex-start">

                        <article class="media">
                            <figure class="media-left">
                                <p class="image is-128x128">
                                    <img src="https://bulma.io/images/placeholders/128x128.png" alt="">
                                </p>
                            </figure>

                            <div class="media-content">
                                <div class="content">
                                    <p>
                                        <strong>Article 1</strong> <small> - Par nom écrivant </small> <small> 05/01/2021 </small>
                                        <br>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.
                                    </p>
                                </div>
                            </div>

                            <div class="media-right">
                                <a href="#">
                                    <span class="icon has-text-fuchsia">
                                        <i class="fas fa-chevron-circle-right"></i>
                                    </span>
                                </a>
                            </div>
                        </article>

                        <article class="media">
                            <figure class="media-left">
                                <p class="image is-128x128">
                                    <img src="https://bulma.io/images/placeholders/128x128.png" alt="">
                                </p>
                            </figure>

                            <div class="media-content">
                                <div class="content">
                                    <p>
                                        <strong>Article 2</strong> <small> - Par nom écrivant </small> <small> 05/01/2021 </small>
                                        <br>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.
                                    </p>
                                </div>
                            </div>

                            <div class="media-right">
                                <a href="#">
                                    <span class="icon has-text-fuchsia">
                                        <i class="fas fa-chevron-circle-right"></i>
                                    </span>
                                </a>
                            </div>
                        </article>

                        <article class="media">
                            <figure class="media-left">
                                <p class="image is-128x128">
                                    <img src="https://bulma.io/images/placeholders/128x128.png" alt="">
                                </p>
                            </figure>

                            <div class="media-content">
                                <div class="content">
                                    <p>
                                        <strong>Article 3</strong> <small> - Par nom écrivant </small> <small> 05/01/2021 </small>
                                        <br>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.
                                    </p>
                                </div>
                            </div>

                            <div class="media-right">
                                <a href="#">
                                    <span class="icon has-text-fuchsia">
                                        <i class="fas fa-chevron-circle-right"></i>
                                    </span>
                                </a>
                            </div>
                        </article>

                        <article class="media">
                            <figure class="media-left">
                                <p class="image is-128x128">
                                    <img src="https://bulma.io/images/placeholders/128x128.png" alt="">
                                </p>
                            </figure>

                            <div class="media-content">
                                <div class="content">
                                    <p>
                                        <strong>Article 4</strong> <small> - Par nom écrivant </small> <small> 05/01/2021 </small>
                                        <br>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.
                                    </p>
                                </div>
                            </div>

                            <div class="media-right">
                                <a href="#">
                                    <span class="icon has-text-fuchsia">
                                        <i class="fas fa-chevron-circle-right"></i>
                                    </span>
                                </a>
                            </div>
                        </article>

                        <article class="media">
                            <figure class="media-left">
                                <p class="image is-128x128">
                                    <img src="https://bulma.io/images/placeholders/128x128.png" alt="">
                                </p>
                            </figure>

                            <div class="media-content">
                                <div class="content">
                                    <p>
                                        <strong>Article 5</strong> <small> - Par nom écrivant </small> <small> 05/01/2021 </small>
                                        <br>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.
                                    </p>
                                </div>
                            </div>

                            <div class="media-right">
                                <a href="#">
                                    <span class="icon has-text-fuchsia">
                                        <i class="fas fa-chevron-circle-right"></i>
                                    </span>
                                </a>
                            </div>
                        </article>

                        <article class="media">
                            <figure class="media-left">
                                <p class="image is-128x128">
                                    <img src="https://bulma.io/images/placeholders/128x128.png" alt="">
                                </p>
                            </figure>

                            <div class="media-content">
                                <div class="content">
                                    <p>
                                        <strong>Article 6</strong> <small> - Par nom écrivant </small> <small> 05/01/2021 </small>
                                        <br>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.
                                    </p>
                                </div>
                            </div>

                            <div class="media-right">
                                <a href="#">
                                    <span class="icon has-text-fuchsia">
                                        <i class="fas fa-chevron-circle-right"></i>
                                    </span>
                                </a>
                            </div>
                        </article>

                        <article class="media">
                            <figure class="media-left">
                                <p class="image is-128x128">
                                    <img src="https://bulma.io/images/placeholders/128x128.png" alt="">
                                </p>
                            </figure>

                            <div class="media-content">
                                <div class="content">
                                    <p>
                                        <strong>Article 7</strong> <small> - Par nom écrivant </small> <small> 05/01/2021 </small>
                                        <br>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.
                                    </p>
                                </div>
                            </div>

                            <div class="media-right">
                                <a href="#">
                                    <span class="icon has-text-fuchsia">
                                        <i class="fas fa-chevron-circle-right"></i>
                                    </span>
                                </a>
                            </div>
                        </article>

                        <article class="media">
                            <figure class="media-left">
                                <p class="image is-128x128">
                                    <img src="https://bulma.io/images/placeholders/128x128.png" alt="">
                                </p>
                            </figure>

                            <div class="media-content">
                                <div class="content">
                                    <p>
                                        <strong>Article 8</strong> <small> - Par nom écrivant </small> <small> 05/01/2021 </small>
                                        <br>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.
                                    </p>
                                </div>
                            </div>

                            <div class="media-right">
                                <a href="#">
                                    <span class="icon has-text-fuchsia">
                                        <i class="fas fa-chevron-circle-right"></i>
                                    </span>
                                </a>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <section class="hero is-dark is-medium is-bold">
        <div class="hero-body">
            <div class="container">
                <div class="columns">
                    <div class="column is-full">
                        <h1 class="title has-text-centered has-text-white"> Avez-vous toujours de questions ? </h1>
                        <p class="subtitle has-text-centered has-text-white">Excellent ! contactez-nous et il nous fera plaisir de vous répondre</p>
                    </div>
                </div>
                <div class="columns is-justify-content-center">
                    <div class="column is-flex is-justify-content-center is-narrow">
                        <a href="contact.php" class="button is-fuchsia-outlined is-large has-text-white"> Contactez-nous </a>
                    </div>
                </div>
            </div>
        </div>
      </section>

    <?php require 'footer.php'; ?>

</body>
</html>