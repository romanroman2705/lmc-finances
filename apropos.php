<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>À propos LMC</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.1/css/bulma.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="https://kit.fontawesome.com/22fdf35712.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="scripts/scripts.js"></script>
  </head>

<body>
    <section class="hero is-fullheight is-dark hero-apropos-presentation">
        <div class="hero-head">
            <?php require 'menu.php'; ?>
        </div>
        <div class="hero-body">
          <div class="container">
            <div class="columns is-justify-content-start">
              <div class="column is-three-fifths">
                <h1 class="is-size-big is-size-1-mobile has-text-fuchsia has-text-weight-bold"> À propos </h1>
                <h1 class="is-size-big is-size-1-mobile has-text-weight-bold">de notre compagnie</h1>
                <p class="subtitle">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Assumenda 
                  consequatur impedit nobis similique adipisci quibusdam, 
                  nam enim doloribus dolore, quo quas ducimus nesciunt earum, dolorem qui eum reprehenderit. Nostrum, ipsam.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="hero-footer">

        </div>
    </section>

    <section class="hero mb-5">

        <div class="hero-body">
            <div class="container is-flex is-align-self-center">
                <div class="columns">
                    <div class="column">
                        <h1 class="title"> Qui nous sommes ? </h1>
                        <h2 class="subtitle mt-2 has-text-justified">
                            this a subtitle
                        </h2>
                        <p class="has-text-justified">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis nulla maxime
                             laboriosam maiores sunt rerum atque odit ipsam harum accusamus laudantium, 
                            perspiciatis veritatis nihil illo sit? Temporibus provident possimus nisi?

                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis nulla maxime
                             laboriosam maiores sunt rerum atque odit ipsam harum accusamus laudantium, 
                            perspiciatis veritatis nihil illo sit? Temporibus provident possimus nisi?

                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis nulla maxime
                             laboriosam maiores sunt rerum atque odit ipsam harum accusamus laudantium, 
                            perspiciatis veritatis nihil illo sit? Temporibus provident possimus nisi?
                        </p>
                    </div>
                    <div class="column is-flex is-justify-content-center">
                        <figure class="image apropos-logo">
                            <img src="images/LMC.png">
                        </figure>
                    </div>
                </div> 
            </div>  
        </div>

    </section>

    <!-- SECTION TEAM --> 

    <section class="hero">
      <div class="hero-body">
        <div class="container">
          <div class="columns is-justify-content-center">

            <div class="column is-one-fifth">
              <div class="card">
                <div class="card-image">
                  <figure class="image is-4by3">
                    <img src="images/professional-1.png" alt="Placeholder image">
                  </figure>
                </div>
                <div class="card-content">

                  <div class="media">
                    <div class="media-content">
                      <p class="title is-4">Nom et prenom</p>
                      <p class="subtitle is-6">Poste de travail</p>
                    </div>
                  </div>

                  <div class="content">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Phasellus nec iaculis mauris.
                  </div>


                </div>
              </div>
            </div>

            <div class="column is-one-fifth">
              <div class="card">
                <div class="card-image">
                  <figure class="image is-4by3">
                    <img src="images/professional-2.png" alt="Placeholder image">
                  </figure>
                </div>
                <div class="card-content">

                  <div class="media">
                    <div class="media-content">
                      <p class="title is-4">Nom et prenom</p>
                      <p class="subtitle is-6">Poste de travail</p>
                    </div>
                  </div>

                  <div class="content">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Phasellus nec iaculis mauris.
                  </div>


                </div>
              </div>
            </div>

            <div class="column is-one-fifth">
              <div class="card">
                <div class="card-image">
                  <figure class="image is-4by3">
                    <img src="images/professional-1.png" alt="Placeholder image">
                  </figure>
                </div>
                <div class="card-content">

                  <div class="media">
                    <div class="media-content">
                      <p class="title is-4">Nom et prenom</p>
                      <p class="subtitle is-6">Poste de travail</p>
                    </div>
                  </div>

                  <div class="content">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Phasellus nec iaculis mauris.
                  </div>


                </div>
              </div>
            </div>

            <div class="column is-one-fifth">
              <div class="card">
                <div class="card-image">
                  <figure class="image is-4by3">
                    <img src="images/professional-2.png" alt="Placeholder image">
                  </figure>
                </div>
                <div class="card-content">

                  <div class="media">
                    <div class="media-content">
                      <p class="title is-4">Nom et prenom</p>
                      <p class="subtitle is-6">Poste de travail</p>
                    </div>
                  </div>

                  <div class="content">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Phasellus nec iaculis mauris.
                  </div>


                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </section>


    <!-- SECTION PARTENAIRES -->
    <section class="hero hero-accueil-partenaires">
        <div class="hero-body">
            <div class="container">
                <div class="columns">
                    <div class="column is-full">
                        <h1 class="title has-text-centered"> Quelqu'uns de nos partenaires</h1>
                        <p class="subtitle has-text-centered"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, sit p</p>
                    </div>
                </div>
                <div class="columns">
                    <div class="column is-flex is-justify-content-center is-one-quarter">
                        <figure class="image is-250x250">
                            <img src="images/logo_SFL_Placements.png" alt="">
                        </figure>
                    </div>
                    <div class="column is-flex is-justify-content-center is-one-quarter">
                        <figure class="image is-250x250">
                            <img src="images/logo_SFL_Placements.png" alt="">
                        </figure>
                    </div>
                    <div class="column is-flex is-justify-content-center is-one-quarter">
                        <figure class="image is-250x250">
                            <img src="images/logo_SFL_Placements.png" alt="">
                        </figure>
                    </div>
                    <div class="column is-flex is-justify-content-center is-one-quarter">
                        <figure class="image is-250x250">
                            <img src="images/logo_SFL_Placements.png" alt="">
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php require 'footer.php'; ?>

</body>
</html>