<?php

$enviado = false;
$error = '';
if(isset($_POST["submit"])) {
	$to = "romanroman2705@gmail.com";
  $from = $_POST['email'];
  $name = $_POST['name'];
	$message = $_POST['message'];

	if(!empty($name)) {
		$name = trim($name);
		$name = filter_var($name, FILTER_SANITIZE_STRING);
	}	else {
		$error .='Entrez votre nom s\'il vous plaît </br>';
	}

	if(!empty($from)) {
		$from = filter_var($from, FILTER_SANITIZE_EMAIL);
		if(!filter_var($from, FILTER_VALIDATE_EMAIL)){
			$error .= 'Entrez un email valide s\'il vous plaît </br>';
		}
	}	else {
		$error .='Entrez votre email s\'il vous plaît </br>';
	}

	if(!empty($message)) {
		$message = trim($message);
		$message = htmlspecialchars($message);
		$message = stripslashes($message);
	}	else {
		$error .='Entrez un message s\'il vous plaît, quelques mots suffiraient </br>';
	}

	if(!$error) {
		$headers = "From: $from";
		$headers = "From: " . $from . "\r\n";
		$headers .= "Reply-To: ". $from . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

		$subject_email = "You have a message from your site web Romantwice.com";

		$link = 'romantwice.com';

		$body = "<!DOCTYPE html><html lang='en'><head><meta charset='UTF-8'><title>Express Mail</title></head><body>";
		$body .= "<table style='width: 100%;'>";
		$body .= "<thead style='text-align: center;'><tr><td style='border:none;' colspan='2'>";
		$body .= "</td></tr></thead><tbody><tr>";
		$body .= "<td style='border:none;'><strong>Name:</strong> {$name}</td></tr>";
		$body .= "<tr><td style='border:none;'><strong>Email:</strong> {$from}</td>";
		$body .= "</tr>";
		$body .= "<tr><td></td></tr>";
		$body .= "<tr><td colspan='2' style='border:none;'>{$message}</td></tr>";
		$body .= "</tbody></table>";
		$body .= "</body></html>";
		
    $send = mail($to, $subject_email, $body, $headers);
    
    $enviado = true;
	}
}
require "views/contact.view.php";
    

?>