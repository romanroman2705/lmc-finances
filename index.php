<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>LMC</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.1/css/bulma.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="https://kit.fontawesome.com/22fdf35712.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="scripts/scripts.js"></script>
  </head>
  <body>
    <section class="hero is-fullheight hero-presentation">
        <div class="hero-head">
            <?php require 'menu.php'; ?>
        </div> 

        <div class="hero-body p-0">
            <div class="container">
                <div class="columns m-0 p-0 is-flex is-flex-direction-column is-align-items-flex-start">
                <div class="column">
                    <p class="is-size-7-mobile is-size-6 has-text-left has-text-white has-text-weight-bold is-uppercase">
                        Nous sommes là pour<br>
                        <span class="is-size-1 is-size-3-mobile has-text-weight-bold has-text-white">
                            FAIRE POUSSER VOTRE PLAN
                            <br>
                            FINANCIER
                        </span>
                    </p>
                    <div class="columns is-mobile is-justify-content-flex-start">
                        <div class="column is-narrow">
                            <a href="#" class="button is-fuchsia"> Nos services </a>
                        </div>
                        <div class="column is-narrow">
                            <a href="#" class="button is-fuchsia"> Contactez-nous </a>
                        </div>
                    </div>
                </div>
                </div> 
            </div>  
        </div>

        <div class="hero-foot">
            <div class="container">
                <div class="columns p-0 m-0 is-mobile is-justify-content-center is-hidden-tablet">
                    <div class="column is-narrow">
                            <span class="icon go-down is-large has-text-fuchsia">
                                <i class="fas fa-3x fa-angle-double-down"></i>
                            </span>
                    </div>
                </div> 
                <div class="columns is-multiline is-mobile m-0 mb-5 is-align-items-center is-justify-content-center is-hidden-mobile">
                    <div class="column is-four-fifths-mobile is-one-third-tablet">
                        <div class="advantage">
                            <p class="title has-text-centered">
                                Avantage
                            </p>
                            <p class="subtitle">
                                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore magnam sapiente odio, optio aliqua 
                            </p>
                        </div>
                    </div>
                    <div class="column is-four-fifths-mobile is-one-third-tablet">
                        <div class="advantage-fuchsia">
                            <p class="title has-text-centered">
                                Avantage
                            </p>
                            <p class="subtitle">
                                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore magnam sapiente odio, optio aliqua 
                            </p>
                        </div>
                    </div>
                    <div class="column is-four-fifths-mobile is-one-third-tablet">
                        <div class="advantage">
                            <p class="title has-text-centered">
                                Avantage
                            </p>
                            <p class="subtitle">
                                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore magnam sapiente odio, optio aliqua 
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- SECTION À PROPOS -->

     <section class="hero mb-5 is-large">

        <div class="hero-body">
            <div class="container is-flex is-align-self-center">
                <div class="columns">
                    <div class="column">
                        <h1 class="title"> Qui nous sommes ? </h1>
                        <h2 class="subtitle mt-2 has-text-justified">
                            this a subtitle
                        </h2>
                        <p class="has-text-justified">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis nulla maxime
                             laboriosam maiores sunt rerum atque odit ipsam harum accusamus laudantium, 
                            perspiciatis veritatis nihil illo sit? Temporibus provident possimus nisi?
                        </p>
                    </div>
                    <div class="column is-flex is-justify-content-center">
                        <figure class="image apropos-logo">
                            <img src="images/LMC.png">
                        </figure>
                    </div>
                </div> 
            </div>  
        </div>

    </section>

    <!-- SECTION SERVICES -->

    <section class="hero is-dark hero-accueil-services">

        <div class="hero-body">
            <div class="container is-flex is-flex-direction-column is-align-self-center">
                <div class="columns is-mobile is-justify-content-center">
                    <div class="column is-narrow is-flex is-flex-direction-column is-align-items-center">
                        <h1 class="is-size-1 has-text-weight-bold has-text-white">Nos services</h1>
                    </div>
                </div> 
                <div class="columns is-justify-content-space-around">
                    <div class="column is-flex is-flex-direction-column is-justify-content-center is-align-items-center is-one-third">
                        <div class="services-background">
                            <img class="image is-128x128" src="images/insurance-icon-white.png">
                        </div>
                        <h1 class="is-size-4 mt-4 has-text-centered has-text-weight-bold"> Assurance de personnes </h1>
                        <div class="underline-fuchsia"></div>
                        <h1 class="is-size-5 has-text-justified"> Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vel reprehenderit culpa, possimus dolore distinctio eaque dignissimos quisquam optios velit debitis. </h1>
                    </div>
                    <div class="column is-flex is-flex-direction-column is-justify-content-center is-align-items-center is-one-third">
                        <div class="services-background">
                            <img class="image is-128x128" src="images/wallet.png">
                        </div>
                        <h1 class="is-size-4 mt-4 has-text-centered has-text-weight-bold"> Gestion de portefeuilles </h1>
                        <div class="underline-fuchsia"></div>
                        <h1 class="is-size-5 has-text-justified"> Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vel reprehenderit culpa, possimus dolore distinctio eaque dignissimos quisquam optios velit debitis. </h1>
                    </div>
                    <div class="column is-flex is-flex-direction-column is-justify-content-center is-align-items-center is-one-third">
                        <div class="services-background">
                            <img class="image is-128x128" src="images/saving-money.png">
                        </div>
                        <h1 class="is-size-4 mt-4 has-text-centered has-text-weight-bold"> Épargne collective et placement </h1>
                        <div class="underline-fuchsia"></div>
                        <h1 class="is-size-5 has-text-justified"> Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vel reprehenderit culpa, possimus dolore distinctio eaque dignissimos quisquam optios velit debitis. </h1>
                    </div>
                </div>
            </div>  
        </div>

    </section>

    <!-- SECTION SERVICES -->
 <!--
    <section class="hero is-light is-bold">

        <div class="hero-body">
            <div class="container is-flex is-flex-direction-column is-align-self-center">
                <div class="columns is-mobile is-justify-content-center">
                    <div class="column is-narrow is-flex is-flex-direction-column is-align-items-center">
                        <h1 class="is-size-2 has-text-weight-bold has-text-dark">Nos services</h1>
                        <div class="underline-fuchsia"></div>
                    </div>
                </div> 
                <div class="columns is-justify-content-space-around">
                    <div class="column is-flex is-flex-direction-column is-justify-content-center is-align-items-center is-one-third">
                        <figure class="image image-services">
                            <img src="images/insurance-icon-white.png">
                        </figure>
                        <h1 class="is-size-4 mt-4 has-text-centered has-text-weight-bold"> Assurance de personnes </h1>
                        <div class="underline-fuchsia"></div>
                        <h1 class="is-size-5 has-text-justified"> Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vel reprehenderit culpa, possimus dolore distinctio eaque dignissimos quisquam optios velit debitis. </h1>
                    </div>
                    <div class="column is-flex is-flex-direction-column is-justify-content-center is-align-items-center is-one-third">
                        <figure class="image image-services">
                            <img src="images/wallet.png">
                        </figure>
                        <h1 class="is-size-4 mt-4 has-text-centered has-text-weight-bold"> Gestion de portefeuilles </h1>
                        <div class="underline-fuchsia"></div>
                        <h1 class="is-size-5 has-text-justified"> Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vel reprehenderit culpa, possimus dolore distinctio eaque dignissimos quisquam optios velit debitis. </h1>
                    </div>
                    <div class="column is-flex is-flex-direction-column is-justify-content-center is-align-items-center is-one-third">
                        <figure class="image image-services">
                            <img src="images/saving-money.png">
                        </figure>
                        <h1 class="is-size-4 mt-4 has-text-centered has-text-weight-bold"> Épargne collective et placement </h1>
                        <div class="underline-fuchsia"></div>
                        <h1 class="is-size-5 has-text-justified"> Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vel reprehenderit culpa, possimus dolore distinctio eaque dignissimos quisquam optios velit debitis. </h1>
                    </div>
                </div>
            </div>  
        </div>

    </section>

-->
    <!-- SECTION ACTUALITES -->

    <section class="hero is-fullheight">
        

        <div class="hero-body">
            <div class="container">
                <div class="columns">
                    <div class="column is-flex is-flex-direction-column is-align-items-center is-justify-content-center">
                        <h1 class="title has-text-centered">
                            Les finances de nos jours
                        </h1>
                        <div class="underline-fuchsia"></div>
                    </div>
                </div>
                <div class="columns">
                    <div class="column">
                        <figure class="image is-4by3">
                            <img src="https://bulma.io/images/placeholders/640x480.png" alt="">
                        </figure>
                        <h1 class="title mt-3"> Article recent titre</h1>
                        <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellat, deserunt nobis vitae dolor tenetur amet, rem totam inventore provident voluptatibus iure cupiditate fugit nemo numquam, tempora in. Non, voluptatem deleniti!
                        </p>
                    </div>
                    <div class="column others-articles is-align-self-flex-start">

                        <article class="media">
                            <figure class="media-left">
                                <p class="image is-128x128">
                                    <img src="https://bulma.io/images/placeholders/128x128.png" alt="">
                                </p>
                            </figure>

                            <div class="media-content">
                                <div class="content">
                                    <p>
                                        <strong>Article 1</strong> <small> - Par nom écrivant </small> <small> 05/01/2021 </small>
                                        <br>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.
                                    </p>
                                </div>
                            </div>

                            <div class="media-right">
                                <a href="#">
                                    <span class="icon has-text-fuchsia">
                                        <i class="fas fa-chevron-circle-right"></i>
                                    </span>
                                </a>
                            </div>
                        </article>

                        <article class="media">
                            <figure class="media-left">
                                <p class="image is-128x128">
                                    <img src="https://bulma.io/images/placeholders/128x128.png" alt="">
                                </p>
                            </figure>

                            <div class="media-content">
                                <div class="content">
                                    <p>
                                        <strong>Article 2</strong> <small> - Par nom écrivant </small> <small> 05/01/2021 </small>
                                        <br>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.
                                    </p>
                                </div>
                            </div>

                            <div class="media-right">
                                <a href="#">
                                    <span class="icon has-text-fuchsia">
                                        <i class="fas fa-chevron-circle-right"></i>
                                    </span>
                                </a>
                            </div>
                        </article>

                        <article class="media">
                            <figure class="media-left">
                                <p class="image is-128x128">
                                    <img src="https://bulma.io/images/placeholders/128x128.png" alt="">
                                </p>
                            </figure>

                            <div class="media-content">
                                <div class="content">
                                    <p>
                                        <strong>Article 3</strong> <small> - Par nom écrivant </small> <small> 05/01/2021 </small>
                                        <br>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.
                                    </p>
                                </div>
                            </div>

                            <div class="media-right">
                                <a href="#">
                                    <span class="icon has-text-fuchsia">
                                        <i class="fas fa-chevron-circle-right"></i>
                                    </span>
                                </a>
                            </div>
                        </article>

                        <article class="media">
                            <figure class="media-left">
                                <p class="image is-128x128">
                                    <img src="https://bulma.io/images/placeholders/128x128.png" alt="">
                                </p>
                            </figure>

                            <div class="media-content">
                                <div class="content">
                                    <p>
                                        <strong>Article 4</strong> <small> - Par nom écrivant </small> <small> 05/01/2021 </small>
                                        <br>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.
                                    </p>
                                </div>
                            </div>

                            <div class="media-right">
                                <a href="#">
                                    <span class="icon has-text-fuchsia">
                                        <i class="fas fa-chevron-circle-right"></i>
                                    </span>
                                </a>
                            </div>
                        </article>

                        <article class="media">
                            <figure class="media-left">
                                <p class="image is-128x128">
                                    <img src="https://bulma.io/images/placeholders/128x128.png" alt="">
                                </p>
                            </figure>

                            <div class="media-content">
                                <div class="content">
                                    <p>
                                        <strong>Article 5</strong> <small> - Par nom écrivant </small> <small> 05/01/2021 </small>
                                        <br>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.
                                    </p>
                                </div>
                            </div>

                            <div class="media-right">
                                <a href="#">
                                    <span class="icon has-text-fuchsia">
                                        <i class="fas fa-chevron-circle-right"></i>
                                    </span>
                                </a>
                            </div>
                        </article>

                        <article class="media">
                            <figure class="media-left">
                                <p class="image is-128x128">
                                    <img src="https://bulma.io/images/placeholders/128x128.png" alt="">
                                </p>
                            </figure>

                            <div class="media-content">
                                <div class="content">
                                    <p>
                                        <strong>Article 6</strong> <small> - Par nom écrivant </small> <small> 05/01/2021 </small>
                                        <br>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.
                                    </p>
                                </div>
                            </div>

                            <div class="media-right">
                                <a href="#">
                                    <span class="icon has-text-fuchsia">
                                        <i class="fas fa-chevron-circle-right"></i>
                                    </span>
                                </a>
                            </div>
                        </article>

                        <article class="media">
                            <figure class="media-left">
                                <p class="image is-128x128">
                                    <img src="https://bulma.io/images/placeholders/128x128.png" alt="">
                                </p>
                            </figure>

                            <div class="media-content">
                                <div class="content">
                                    <p>
                                        <strong>Article 7</strong> <small> - Par nom écrivant </small> <small> 05/01/2021 </small>
                                        <br>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.
                                    </p>
                                </div>
                            </div>

                            <div class="media-right">
                                <a href="#">
                                    <span class="icon has-text-fuchsia">
                                        <i class="fas fa-chevron-circle-right"></i>
                                    </span>
                                </a>
                            </div>
                        </article>

                        <article class="media">
                            <figure class="media-left">
                                <p class="image is-128x128">
                                    <img src="https://bulma.io/images/placeholders/128x128.png" alt="">
                                </p>
                            </figure>

                            <div class="media-content">
                                <div class="content">
                                    <p>
                                        <strong>Article 8</strong> <small> - Par nom écrivant </small> <small> 05/01/2021 </small>
                                        <br>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.
                                    </p>
                                </div>
                            </div>

                            <div class="media-right">
                                <a href="#">
                                    <span class="icon has-text-fuchsia">
                                        <i class="fas fa-chevron-circle-right"></i>
                                    </span>
                                </a>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <!-- SECTION PARTENAIRES -->

    <!--
    <section class="hero">

        <div class="hero-body">
            <div class="container">
                <div class="columns columns-partenaires">
                    <div class="column">
                        <button id="left-slider-button" class="button">
                            <<<                        
                        </button>
                    </div>

                    <div class="column column-logo-partenaires is-four-fifths">

                        <div id="columns-logo-partenaires-1" class="columns is-full">
                            <div id="column-partenaires-1" class="column">
                                <figure class="image is-250x250">
                                    <img src="images/logo_SFL_Placements.png" alt="SFL logo">
                                </figure>
                            </div>
                            <div id="column-partenaires-2" class="column">
                                <figure class="image is-250x250">
                                    <img src="images/logo_SFL_Placements.png" alt="SFL logo">
                                </figure>
                            </div>
                            <div id="column-partenaires-3" class="column">
                                <figure class="image is-250x250">
                                    <img src="images/logo_SFL_Placements.png" alt="SFL logo">
                                </figure>
                            </div>
                        </div>

                        <div id="columns-logo-partenaires-2" class="columns is-full">
                            <div id="column-" class="column">
                                <figure class="image is-250x250">
                                <img src="images/logo_SFL_Placements.png" alt="SFL logo">
                                </figure>
                            </div>
                            <div id="column-part-2" class="column">
                                <figure class="image is-250x250">
                                    <img src="images/logo_SFL_Placements.png" alt="SFL logo">
                                </figure>
                            </div>
                            <div id="colnaires-3" class="column">
                                <figure class="image is-250x250">
                                    <img src="images/logo_SFL_Placements.png" alt="SFL logo">
                                </figure>
                            </div>
                        </div>

                    </div>

                    <div class="column">
                        <button id="right-slider-button" class="button">
                            >>>                        
                        </button>
                    </div>
                </div>
            </div>
        </div>

    </section> 
    -->

    <!-- SECTION PARTENAIRES -->
    <section class="hero has-background-silver">
        <div class="hero-body">
            <div class="container">
                <div class="columns">
                    <div class="column is-full">
                        <h1 class="title has-text-centered"> Quelqu'uns de nos partenaires</h1>
                        <p class="subtitle has-text-centered"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, sit p</p>
                    </div>
                </div>
                <div class="columns">
                    <div class="column is-flex is-justify-content-center is-one-quarter">
                        <figure class="image is-250x250">
                            <img src="images/logo_SFL_Placements.png" alt="">
                        </figure>
                    </div>
                    <div class="column is-flex is-justify-content-center is-one-quarter">
                        <figure class="image is-250x250">
                            <img src="images/logo_SFL_Placements.png" alt="">
                        </figure>
                    </div>
                    <div class="column is-flex is-justify-content-center is-one-quarter">
                        <figure class="image is-250x250">
                            <img src="images/logo_SFL_Placements.png" alt="">
                        </figure>
                    </div>
                    <div class="column is-flex is-justify-content-center is-one-quarter">
                        <figure class="image is-250x250">
                            <img src="images/logo_SFL_Placements.png" alt="">
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- SECTION CONTACT --> 

    <section class="hero hero-accueil-contact is-large">
        <div class="hero-body">
            <div class="container">
                <div class="columns">
                    <div class="column is-full">
                        <h1 class="title has-text-centered has-text-white"> Commençons votre planification financière! </h1>
                        <p class="subtitle has-text-centered has-text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, sit p</p>
                    </div>
                </div>
                <div class="columns is-justify-content-center">
                    <div class="column is-flex is-justify-content-center is-narrow">
                        <a href="#" class="button is-fuchsia-outlined is-large has-text-white"> Contactez-nous </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
   
    <?php require 'footer.php'; ?>
    
  </body>
</html>