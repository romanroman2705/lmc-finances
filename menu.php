<nav class="navbar" role="navigation" aria-label="main navigation">
                <div class="container">
                    <div class="navbar-brand">
                        <a href="index.php" class="navbar-item" href="index.php">
                            <h1 class="title is-size-1 has-text-white is-uppercase">LMC</h1> 
                        </a>

                        <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                        </a>
                    </div>

                    <div id="navbarBasicExample" class="navbar-menu">
                        <div class="navbar-start">

                            <div class="navbar-item">
                                <div class="buttons is-flex is-justify-content-center">
                                <a href="index.php" class="has-text-white">
                                    <strong>Accueil</strong>
                                </a>
                                </div>
                            </div>

                            <div class="navbar-item">
                                <div class="buttons is-flex is-justify-content-center">
                                    <a href="apropos.php" class="has-text-white">
                                        <strong> À propos</strong>
                                    </a>
                                </div>
                            </div>

                            <div class="navbar-item has-dropdown is-hoverable">
                                <div class="buttons is-flex is-flex-direction-column is-justify-content-center ">
                                    <a href="services.php" class="navbar-link has-text-white has-text-weight-bold">
                                        Services
                                    </a>
                                    <div class="navbar-dropdown">
                                        <a class="navbar-item has-text-white">
                                            Assurance de personnes
                                        </a>
                                        <a class="navbar-item has-text-white">
                                            Gestion de portefeuilles
                                        </a>
                                        <a class="navbar-item has-text-white">
                                            Épargne collective et placements
                                        </a>
                                </div>
                                </div>
                            </div>
                            
                            <div class="navbar-item">
                                <div class="buttons is-flex is-justify-content-center">
                                    <a href="actualites.php" class="has-text-white">
                                        <strong> Actualités </strong>
                                    </a>
                                </div>
                            </div>

                            <div class="navbar-item">
                                <div class="buttons is-flex is-justify-content-center">
                                    <a href="faq.php" class="has-text-white">
                                        <strong> FAQ </strong>
                                    </a>
                                </div>
                            </div>

                        </div>

                        <div class="navbar-end">
                            <div class="navbar-item">
                                <div class="buttons is-flex is-justify-content-center">
                                    <a href="contact.php" class="button is-fuchsia">
                                        <strong>Nous joindre</strong>
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </nav>