document.addEventListener('DOMContentLoaded', () => {

    const navbarBurger = document.querySelector('.navbar-burger');

    // Check if there are any navbar burgers
    if (navbarBurger) {
  
      // Add a click event on each of them
      navbarBurger.addEventListener('click', () => {
        const target = document.querySelector('.navbar-menu');
        target.classList.toggle('is-active');

      });
    }

    if (window.screen.width <= 1023) {
      const servicesButton = document.querySelector('.has-dropdown .navbar-link');
      servicesButton.addEventListener("click", () => {
        const dropdown = document.querySelector(".navbar-dropdown");
        dropdown.classList.toggle("is-visible");
      });
    }

    /* ------------ FAQ ---------- */
    const questions = document.querySelectorAll('.question p');

    for (let i = 0; i < questions.length; i += 1) {
      questions[i].addEventListener('click', () => {
        const responseSelector = '#response-' + (i + 1);  
        const response = document.querySelector(responseSelector);
        if (response) {
          response.classList.toggle('is-hidden');
        }
      });
    }

  });

/* ------------- MAP ------------- */

  let map;

  const mapStylesArray = [
    {
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#f5f5f5"
        }
      ]
    },
    {
      "elementType": "labels.icon",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#616161"
        }
      ]
    },
    {
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#f5f5f5"
        }
      ]
    },
    {
      "featureType": "administrative.land_parcel",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#bdbdbd"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#eeeeee"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#757575"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#e5e5e5"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#9e9e9e"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#ffffff"
        }
      ]
    },
    {
      "featureType": "road.arterial",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#757575"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#dadada"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#616161"
        }
      ]
    },
    {
      "featureType": "road.local",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#9e9e9e"
        }
      ]
    },
    {
      "featureType": "transit.line",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#e5e5e5"
        }
      ]
    },
    {
      "featureType": "transit.station",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#eeeeee"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#c9c9c9"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#9e9e9e"
        }
      ]
    }
  ]

  function initMap() {

    const office = { lat: 45.521265993471836, lng: -73.3323637646352 };
    
    map = new google.maps.Map(document.getElementById("map"), {
      center: office,
      zoom: 12,
      styles: mapStylesArray,
      
    });

    // The marker, positioned on the office
    const marker = new google.maps.Marker({
      position: office,
      map: map,
      icon: 'images/marker.png'
    });
  }