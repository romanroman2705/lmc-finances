<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contact LMC</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.1/css/bulma.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="https://kit.fontawesome.com/22fdf35712.js" crossorigin="anonymous"></script>
    <script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDrN0ofnei7Krmjb3U1DQhq8n-nToQKRoQ&callback=initMap&libraries=&v=weekly"
      defer
    ></script>
<script type="text/javascript" src="scripts/scripts.js"></script>
  </head>

<body>
    <section class="hero is-fullheight is-dark hero-contact-presentation">

        <div class="hero-head">
            <?php require 'menu.php'; ?>
        </div>

        <div class="hero-body">
          <div class="container">
              <div class="columns is-justify-content-start">
                <div class="column is-three-fifths">
                  <h1 class="is-size-big is-size-1-mobile has-text-fuchsia has-text-weight-bold"> Contactez-nous </h1>
                  <h1 class="is-size-big is-size-1-mobile has-text-weight-bold"> Pour débuter notre collaboration </h1>
                  <p class="subtitle">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Assumenda 
                    consequatur impedit nobis similique adipisci quibusdam, 
                    nam enim doloribus dolore, quo quas ducimus nesciunt earum, dolorem qui eum reprehenderit. Nostrum, ipsam.</p>
                </div>
              </div>
            </div>
        </div>

        <div class="hero-footer">

        </div>
    </section>

    <section class="hero">
        <div class="hero-body">
            <div class="container">
                <div class="columns is-mobile is-multiline is-align-items-center is-justify-content-space-around">
                    <div class="column is-narrow">
                        <article class="media no-border">
                            <figure class="media-left">
                                <p class="image is-32x32">
                                    <img src="images/marker.png">
                                </p>
                            </figure>
                            <div class="media-content">
                                <div class="content">
                                    <p>1990 rue de Montarville,<br>
                                        Bureau 200#, Saint-Bruno-de-Montarville,<br> 
                                        QC, Canada<br>
                                        H1W 1L2 
                                    </p>
                                </div>
                            </div>
                        </article>

                        <article class="media no-border">
                            <figure class="media-left">
                                <p class="image is-32x32">
                                    <img src="images/telephone.png">
                                </p>
                            </figure>
                            <div class="media-content">
                                <div class="content">
                                    <p>Téléphone: 450 676-7873 <br>
                                        Télécopie: 450 676-7873 <br>
                                        Sans frais:  450 676-7873
                                    </p>
                                </div>
                            </div>
                        </article>

                        <article class="media no-border">
                            <figure class="media-left">
                                <p class="image is-32x32">
                                    <img src="images/info.png">
                                </p>
                            </figure>
                            <div class="media-content">
                                <div class="content">
                                    <p>Courriel: exemple@gmail.com <br>
                                        <a href="#"> Facebook - LMC finances </a> <br>
                                        <a href="#"> Linkedin - Lisa McCulloch </a> <br>
                                        <a href="#"> Instagram - LMC finances </a>
                                    </p>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="column is-full-mobile is-three-fifths-tablet">
                        <div id="map"></div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <section class="hero is-medium is-dark is-bold">
        <div class="hero-body"> 
            <div class="container">

                <div class="columns">
                    <div class="column">
                        <h1 class="is-size-2 is-size-3-mobile has-text-white has-text-weight-bold has-text-centered"> 
                            Écrivez-nous !
                        </h1>
                    </div>
                </div>

                <div class="columns is-mobile is-justify-content-center mt-5">
                    <div class="column is-four-fifths-mobile is-half-tablet">
                    
                        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post">

                        <div class="field is-horizontal">
                            <div class="field-body">
                                
                                <div class="field">
                                    <p class="control is-expanded has-icons-left">
                                    <input class="input is-bordered" type="text" placeholder="Prenom" name="first_name">
                                    <span class="icon is-small is-left">
                                        <i class="fas fa-user"></i>
                                    </span>
                                    </p>
                                </div>

                                <div class="field">
                                    <p class="control is-expanded has-icons-left">
                                    <input class="input is-bordered" type="text" placeholder="Nom de famille" name="last_name">
                                    <span class="icon is-small is-left">
                                        <i class="fas fa-user"></i>
                                    </span>
                                    </p>
                                </div>

                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-body"> 

                                <div class="field">
                                    <p class="control is-expanded has-icons-left">
                                    <input class="input is-bordered" type="email" placeholder="Email" name="email">
                                    <span class="icon is-small is-left">
                                        <i class="fas fa-envelope"></i>
                                    </span>
                                    </p>
                                </div>

                                <div class="field">
                                    <p class="control is-expanded has-icons-left">
                                    <input class="input is-bordered" type="tel" placeholder="Téléphone" name="telephone">
                                    <span class="icon is-small is-left">
                                        <i class="fas fa-phone"></i>
                                    </span>
                                    </p>
                                </div>

                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-body">

                                <div class="field">
                                    <div class="control">
                                    <textarea name="message" class="textarea" placeholder="Une brève explication de votre projet"></textarea>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-body is-flex-direction-column is-justify-content-center">
                            <div class="field-label is-normal has-text-left"> 
                                <?php if($error !== '') : ?>
                                    <label class="label has-text-danger">
                                        <?php echo $error; ?>
                                    </label>
                                <?php elseif($enviado === true) : ?>
                                    <label class="label has-text-success">
                                        Merci de me contacter!
                                    </label>
                                    <label class="label">
                                        Il me fera plaisir de collaborer avec vous, je vous contacterai sous peu
                                    </label>
                                <?php endif ?>
                            </div>
                            </div>
                        </div>

                        <div class="field is-flex is-justify-content-center mt-5">
                            <div class="control">
                            <button name="submit" class="button is-fuchsia-outlined is-large">
                                Envoyer
                            </button>
                            </div>
                        </div>
                        </form>
                    
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php require 'footer.php'; ?>

</body>
</html>





















